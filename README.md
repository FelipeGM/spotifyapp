# SpotifyApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Instalacion modulos
Ejecutar `npm install` en la carpeta del proyecto

## Development server

Ejecutar `ng serve` para subir el servidor de desarrollo. Ir a la pagina `http://localhost:4200/`.

## Manejo de token de sesion

En la carpeta shared/services esta el servicio spotify-api.service.ts, en donde se codifico un metodo validAuth y auht.

	- La funcion "Auth" permite generar un nuevo token consumiendo el servicio de spotify y posteriormente guarda el token en el localStorage con el nombre "token"
	- La funcion "validAuth" permite validar si el token existe en el localStorage, si no existe ejecuta la funcion "Auth" y crea el token en el localStorage.
	  En el caso del que el token ya exista se procede a hacer a una llamada al servicio de "nuevos lanzamientos" en el caso de no poder consumir el servicio,
	  la funcion "handleError" manejara el error y en el caso de obtener un "status code" igual a 401 se eliminara el local storage y se recargara la pagina para que se
	  re-ejecute el metodo de validacion y cree un nuevo token.