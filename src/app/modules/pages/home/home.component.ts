import {Component, OnInit} from '@angular/core';
import {SpotifyApiService} from '../../../shared/services/spotify-api.service'
import {Auth} from 'src/app/shared/model/auth';
import {Lanzamientos} from 'src/app/shared/model/lanzamientos';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private service: SpotifyApiService) {
  }

  token: Auth;
  nuevosLanzamientos: Lanzamientos;

  ngOnInit() {
    this.service.validateAuth();
    this.getNuevlosLanzamientos();
  }



  auth() {
    return this.service.getAuth().subscribe((data: Auth) => {
      this.token = data;
      localStorage.setItem("token",this.token.access_token+"");
      this.getNuevlosLanzamientos();
      
    });
  }

  getNuevlosLanzamientos() {
    return this.service.getNuevosLanzamientos(localStorage.getItem("token"))
      .subscribe((data: Lanzamientos) => {
        this.nuevosLanzamientos = data;
        console.log(this.nuevosLanzamientos);
      })
  }

}
