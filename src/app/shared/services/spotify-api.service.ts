import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Auth } from '../model/auth';
import { Lanzamientos, Albums } from '../model/lanzamientos';
import { Artistas } from '../model/artistas';

@Injectable({
  providedIn: 'root'
})
export class SpotifyApiService {


  private baseUrl: String = "https://api.spotify.com/v1";
  private clientId: string = environment.clientId;
  private clientSecret: string = environment.clientSecret;

  constructor(private http: HttpClient) {
  }

  getAuth = () => {

    let params: HttpParams = new HttpParams();
    params.set('grant_type', 'client_credentials');

    return this.http.post<Auth>(
      'https://accounts.spotify.com/api/token', 'grant_type=client_credentials', {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa(this.clientId + ":" + this.clientSecret)
      }
    })
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }


  validateAuth = () => {
    console.log(localStorage.getItem("token"))
    if (localStorage.getItem("token") == null) {
        this.getAuth().subscribe((data: Auth) => {
        localStorage.setItem("token", data.access_token + "");
      });
    } else {
     
   this.http.get<any>
     (this.baseUrl + '/browse/new-releases',
       { observe: 'response', 
       headers: { 'Authorization': 'Bearer ' + localStorage.getItem("token") } 
      }).pipe(catchError(this.handleError)).subscribe(response => {
        console.log("Entra aca");
          if(response.status === 401){
           
          }
          console.log(response);
        }
        );
        
    }
return;
  }

  getNuevosLanzamientos(token: String): Observable<{}> {

    return this.http.get<Lanzamientos>(this.baseUrl + '/browse/new-releases', { headers: { 'Authorization': 'Bearer ' + token } })
      .pipe(
        
        catchError(this.handleError)
      );
  }

  getArtista(id: String, token: String): Observable<Artistas> {
    return this.http.get<Artistas>(this.baseUrl + '/artists/' + id, { headers: { 'Authorization': 'Bearer ' + token } })
      .pipe(
       
        catchError(this.handleError)
      );
  }

  getAlbumsArtista(id: String, token: String) {
    return this.http.get<Albums>(this.baseUrl + '/artists/' + id + "/albums", { headers: { 'Authorization': 'Bearer ' + token } })
      .pipe(
       
        catchError(this.handleError)
      );
  }


  getAlbumsTracks(id: String, token: String) {
    return this.http.get<any>(this.baseUrl + '/albums/' + id, { headers: { 'Authorization': 'Bearer ' + token } })
      .pipe(
        
        catchError(this.handleError)
      );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      if(error.status == 401){
        localStorage.clear();
        console.log("Token expirado, generando nuevo token");
        alert("Token expirado, generando nuevo token");
        location.reload();
      }
    }
    //window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
