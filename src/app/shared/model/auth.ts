export class Auth {
  access_token: String;
  token_type: String;
  expires_in: Number;
}

