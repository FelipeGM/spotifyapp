import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './modules/pages/home/home.component';
import {ArtistaComponent} from './components/common/artista/artista.component';


const routes: Routes = [
  {path: '', redirectTo: '/lanzamientos', pathMatch: 'full'},
  {path: 'lanzamientos', component: HomeComponent},
  {path: 'artista/:id', component: ArtistaComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
