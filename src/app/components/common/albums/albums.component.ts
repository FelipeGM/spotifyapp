import {Component, OnInit, Input} from '@angular/core';
import {Item} from 'src/app/shared/model/lanzamientos';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {

  @Input() albums: Item[] = [];

  constructor() {
  }

  ngOnInit() {
  }

}
