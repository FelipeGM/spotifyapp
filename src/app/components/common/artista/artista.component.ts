import {Component, OnInit} from '@angular/core';
import {Artistas} from 'src/app/shared/model/artistas';
import {ActivatedRoute} from "@angular/router";
import {SpotifyApiService} from 'src/app/shared/services/spotify-api.service';
import {Auth} from 'src/app/shared/model/auth';
import {Albums} from 'src/app/shared/model/lanzamientos';


@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styleUrls: ['./artista.component.css']
})
export class ArtistaComponent implements OnInit {

  artista: Artistas;
  albums: Albums;
  idArtista: String;
  token: Auth;

  cancion: any;

  constructor(private route: ActivatedRoute, private service: SpotifyApiService) {
  }

  ngOnInit() {
    this.service.validateAuth();
    this.getArtista(this.route.snapshot.paramMap.get("id"), localStorage.getItem("token"));
    this.getAlbumsArtista(this.route.snapshot.paramMap.get("id"), localStorage.getItem("token"))
  
 
  }

 
  getArtista(id: String, token: String) {
    return this.service.getArtista(id, token)
      .subscribe((data: Artistas) => {
        this.artista = data;
        console.log(this.artista)
      })
  }

  getAlbumsArtista(id: String, token: String) {
    return this.service.getAlbumsArtista(id, token)
      .subscribe((data: Albums) => {
        this.albums = data;

        this.albums.items.forEach(element => {
          this.service.getAlbumsTracks(element.id, token)
            .subscribe((data: any) => {
              this.cancion = data;
              element.song = this.cancion.tracks.items[0].name;
            });
        });

      });
  }

}
